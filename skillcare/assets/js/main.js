'use strict';

$(document).ready(function () {
    $("#nav_courses>li").click(function () {
        $(this).addClass("active").siblings().removeClass("active");

        if($("#nav_courses>li:first-child").hasClass("active")) {
            $("#course_left").addClass('disabled');
        } else {
            $("#course_left").removeClass('disabled');
        }
        if($("#nav_courses>li:last-child").hasClass("active")) {
            $("#course_right").addClass('disabled');
        } else {
            $("#course_right").removeClass('disabled');
        }
    });
    $("#course_left").click(function () {
        if($("#nav_courses>li.active").prev().length) {
            $("#nav_courses>li.active").removeClass("active").prev().addClass("active");
        }

        if($("#nav_courses>li:first-child").hasClass("active")) {
            $("#course_left").addClass('disabled');
        } else {
            $("#course_left").removeClass('disabled');
        }
        if($("#nav_courses>li:last-child").hasClass("active")) {
            $("#course_right").addClass('disabled');
        } else {
            $("#course_right").removeClass('disabled');
        }
    });
    $("#course_right").click(function () {
        if($("#nav_courses>li.active").next().length) {
            $("#nav_courses>li.active").removeClass("active").next().addClass("active");
        }

        if($("#nav_courses>li:first-child").hasClass("active")) {
            $("#course_left").addClass('disabled');
        } else {
            $("#course_left").removeClass('disabled');
        }
        if($("#nav_courses>li:last-child").hasClass("active")) {
            $("#course_right").addClass('disabled');
        } else {
            $("#course_right").removeClass('disabled');
        }
    });

    $('#start_video').click( function(){
        $('#start_video').hide();
        $('#video').get(0).play();
        $('#video').attr('controls', true);
    })

    //burger menu
    $('#burger').click(function(){
        $('.header-content').toggleClass('header-content_mobile-active');
        $(".main").toggleClass('menu-active');
    })
});
