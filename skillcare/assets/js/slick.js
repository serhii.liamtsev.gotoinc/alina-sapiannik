'use strict';

$(document).ready(function(){
    $('.slick-slider').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
    });
});