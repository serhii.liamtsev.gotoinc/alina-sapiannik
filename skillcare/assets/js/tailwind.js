"use strict";

// Use prettier in this file, indentations are broken

tailwind.config = {
  theme: {
    extend: {
      colors: {
        // Best practice is to name colors in kebab-case
        // Instead "dark-text" => "dark-text", it is more css-way
        "dark-text": "#3D3D3D",
        "dark-link": "#4D4D56",
        "orange-text": "#FE7244",
        "violet": "#6652DA",
        "light-violet": "#E8E3FC",
        "light-violet-gray": "#DDDDEE",
        "light-gray-bg": "#F5F5F5",
        "off-white-bg": "#FAFAFA",
        "box-shadow": "rgba(31, 31, 51, 0.06)",
      },
      backgroundImage: {
        "man-w-laptop": "url('assets/img/join/man-w-laptop.png')",
      },
    },
    fontFamily: {
      'nunito': ['Nunito', 'sans-serif'],
    }
  },
};
